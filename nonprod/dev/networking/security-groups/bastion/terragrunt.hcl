# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/security-group.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name   = "${local.account_name}-${local.env}-bastion"
  vpc_id = dependency.vpc.outputs.vpc_id

  ingress_with_cidr_blocks = [
    {
      cidr_blocks = "127.0.0.1/32"
      rule        = "ssh-tcp"
      protocol    = "tcp"
    }
  ]

  egress_with_cidr_blocks = [
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]
}

## Nothing to change below this line

# These are dependecies from other configs

dependency "vpc" {
  config_path = "../../vpc"

  mock_outputs = {
    vpc_id = "vpc-id-mock"
  }
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
